import * as React from "react";
import {useCallback, useState, useEffect} from "react";
import { Container } from "react-bootstrap";
import Button from 'react-bootstrap/Button';
import { Link } from 'react-router-dom';
import {useDropzone} from 'react-dropzone';
import {useHistory} from 'react-router-dom';
import { connect } from 'react-redux';


function mapStateToProps(state) {
    return {
      nom: state.nom,
      text: state.text
    };
}


function Basic(props) {
    const [files, setFiles] = useState([]);
    const[nomFitx, setNom] = useState("");
    const[textFitx, setText] = useState("");
    let history = useHistory();

    useEffect(() => { 
        props.props.dispatch({type: "AFEGIR",  nomfitxer: nomFitx, text:textFitx}); 
    });

    const onDrop = useCallback((acceptedFiles,props) => {
        
        acceptedFiles.forEach((file) => {
        const reader = new FileReader()
        var binaryStr = '';
        reader.onabort = () => console.log('file reading was aborted')
        reader.onerror = () => console.log('file reading has failed')
        reader.onload = (props) => {
        // Do whatever you want with the file contents
            binaryStr = reader.result;
            
            setNom(file.name);
            setText(binaryStr);
            setTimeout(() => {history.push({pathname:'/editor', state: {fitxerNom: file.name, fitxerContingut: binaryStr}})}, 250);
        }
       
        reader.readAsText(file)
        binaryStr = reader.result;
        setFiles(acceptedFiles);
        
        })
        

    }, [])
    const { 
        getRootProps, 
        getInputProps} = useDropzone({onDrop});

    return (
        <section className="container">
        <div {...getRootProps({className: 'dropzone'})}>
                <input {...getInputProps()} ></input>
            <Button variant="outline-dark ml-1 mt-2" >Obrir fitxer</Button>
        </div>
        </section>     
    );
}

const Header = (props) => {
    
    return (
        <Container className="mt-4">
            <h5>Benvingut, tria què vols fer.</h5>
            <Link to={{pathname:'/editor', state: {fitxerNom: '', fitxerContingut: ''}}}>
                <Button variant="outline-light">Fitxer nou</Button>
            </Link>
            <Basic props={props}/>

        </Container>
        )
}
//dadesFitxer={this.props}
export default connect(mapStateToProps)(Header)