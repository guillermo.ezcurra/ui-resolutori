import * as React from "react";
import { Form, Button, Modal } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

function mapStateToProps(state) {
    console.log(state);
    return {
      nom: state.fitxers.nom,
      text: state.fitxers.text,
      valorNomBD: state.basededades.nomBD,
      valorURL: state.basededades.urlBD,
      valorUsr: state.basededades.usrBD,
      valorClau: state.basededades.clauBD
    };
}



class ConnexioBD extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isConnected: false,
            esCorrecta: '',    
            valorURL: '',
            valorUsr: '',
            valorClau: '',
            valorNomBD: '',
            prevPath: this.props.history.location.state.prevPath,
            text: this.props.text,
            nom: this.props.nom
        };
        this.connectarBaseDeDades = this.connectarBaseDeDades.bind(this);
        this.handleClauChange = this.handleClauChange.bind(this);
        this.handleURLChange = this.handleURLChange.bind(this);
        this.handleUsrChange = this.handleUsrChange.bind(this);
        this.handleNomBDChange = this.handleNomBDChange.bind(this);
        this.provarConnexio = this.provarConnexio.bind(this);
        
    }
    async componentWillUnmount() {

        if (this.state.isConnected) await this.props.dispatch({type: "AFEGIRBD",  nomBD: this.state.valorNomBD, urlBD:this.state.valorURL, usrBD:this.state.valorUsr, clauBD:this.state.valorClau});
      }
    connectarBaseDeDades(event) {
        const axios = require('axios');
        let correctesa = this;
        console.log("ValorURL:"+ this.state.prevPath);
        const incorrecta = "Dades de connexió incorrectes";
        //const history = useHistory();
        //Gestió de la connexió a la base de dades
        var base64 = require('base-64');
        var clauCodificada = base64.encode(this.state.valorClau);
        axios.get(`http://localhost:8080/api/bd`, {
            params: {
            nomBD: this.state.valorNomBD,
            URL: this.state.valorURL,
            username: this.state.valorUsr,
            clau: clauCodificada
        }})
      .then(function (response) {
        if(response.data === "Connexió correcta") {
            correctesa.setState({isConnected: true});
        }
        else correctesa.setState({esCorrecta: incorrecta});
        
      });
    }
    handleURLChange(event) {
        this.setState({valorURL: event.target.value});
    }
    handleUsrChange(event) {
        this.setState({valorUsr: event.target.value});
    }
    handleClauChange(event) {
        this.setState({valorClau: event.target.value});
    }
    handleNomBDChange(event) {
        this.setState({valorNomBD: event.target.value});
    }
    handleSubmit = (event) => {
        this.connectarBaseDeDades(event);
    }
    handleTest = (event) => {
        this.provarConnexio(event);
    }
    provarConnexio = (event) => {
        const axios = require('axios');
        console.log("ValorURL:"+ this.state.valorClau);
        let correctesa = this;
        var base64 = require('base-64');
        var clauCodificada = base64.encode(this.state.valorClau);
        //Gestió de la connexió a la base de dades
        axios.get(`http://localhost:8080/api/bd`, {
            params: {
            nomBD: this.state.valorNomBD,
            URL: this.state.valorURL,
            username: this.state.valorUsr,
            clau: clauCodificada
        }})
      .then(function (response) {
        const error = response.data;
        const valida = "Connexió vàlida";
        const incorrecta = "Dades de connexió incorrectes";
        console.log(error);
        if(response.data === "Connexió correcta") correctesa.setState({esCorrecta: valida});
        else correctesa.setState({esCorrecta: incorrecta});
      });
    }
    //const dbClassName = "com.mysql.cj.jdbc.Driver";
    render() {
        if(this.state.isConnected) {
            return <Redirect to = {{ pathname: "/editor", state: {fitxerNom: this.props.nom, fitxerContingut: this.props.text} }} />;
        }
        else return (
            <Form onSubmit={this.connectarBaseDeDades}>
                <Modal show={true}>
                    <Modal.Header closeButton onClick={() => this.props.history.push({pathname:'/editor', state: {fitxerNom: this.props.nom, fitxerContingut: this.props.text}})}>
                    
                        Connexió a la base de dades
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Group controlId="nomBD">
                            <Form.Label>Nom de la base de dades</Form.Label>
                            <Form.Control type="text" value={this.state.valorNomBD} placeholder="Nom de la BD" onChange={this.handleNomBDChange}/>
                        </Form.Group>
                        <Form.Group controlId="urlBD">
                            <Form.Label>URL</Form.Label>
                            <Form.Control type="text" value={this.state.valorURL} placeholder="Adreça de la BD" onChange={this.handleURLChange}/>
                        </Form.Group>
                        <Form.Group controlId="usuariBD">
                            <Form.Label>Nom d'usuari</Form.Label>
                            <Form.Control type="text" value={this.state.valorUsr} placeholder="Nom d'usuari" onChange={this.handleUsrChange}/>
                        </Form.Group>
                        <Form.Group controlId="clauBD">
                            <Form.Label>Clau d'accés</Form.Label>
                            <Form.Control type="password" value={this.state.valorClau}  placeholder="Clau" onChange={this.handleClauChange}/>
                        </Form.Group>
                        {this.state.esCorrecta}
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="dark" onClick={this.handleTest}>
                            Provar
                        </Button>
                        <Button variant="outline-dark" type="submit" onClick={this.handleSubmit}>
                            Connectar
                        </Button>
                    </Modal.Footer>
                </Modal>
            </Form>
        )
    }
}
export default connect(mapStateToProps)(ConnexioBD);