import * as React from "react";
import { Form, Button, Modal } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';


const IniciSessio = () => {
    const history = useHistory();
    return (
        <Form>
            <Modal show={true}>
                <Modal.Header closeButton onClick={() => history.push('/')}>
                
                    Inici de sessió
                </Modal.Header>
                <Modal.Body>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Nom d'usuari</Form.Label>
                        <Form.Control type="text" placeholder="Nom d'usuari" />
                    </Form.Group>

                    <Form.Group controlId="formBasicPassword">
                        <Form.Label>Clau d'accés</Form.Label>
                        <Form.Control type="password" placeholder="Clau" />
                    </Form.Group>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="outline-dark" type="submit">
                        Entrar
                    </Button>
                </Modal.Footer>
            </Modal>
        </Form>
    )
}
export default IniciSessio