import React from 'react';
import './App.css';
import Container from 'react-bootstrap/Container';
import { Route } from 'react-router-dom';
import Registre from '../Registre/Registre.js';
import Inici from '../Inici/Inici.js';
import IniciSessio from '../IniciSessio/IniciSessio.js';
import PaginaEdicio from "../Editor/PaginaEdicio.js";
import ConnexioBD from "../ConnexioBD/ConnexioBD.js";

function App() {
  return (
    
    <Container fluid={true} className="App" style={{ paddingLeft: 0, paddingRight: 0 }}>
        <Route exact={true} path='/' component={Inici}/>
        <Route path='/registre' component={Registre}/>
        <Route path='/inicisessio' component={IniciSessio}/>
        <Route path='/connexiobd' render={(props) => <ConnexioBD {...props}/>}/>
        <Route path='/editor' render={(props) => <PaginaEdicio {...props}/>}/>
    </Container>
  );
}
export default App;
