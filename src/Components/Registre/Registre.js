import * as React from "react";
import { Form, Button } from 'react-bootstrap';
import './Registre.css'

const Registre = () => {
    return (
        <div className="auth-wrapper mt-4">
            <div className="auth-inner">
            <Form>
                <h4> Registre </h4>
                <Form.Group controlId="formBasicEmail">
                    <Form.Label>Nom d'usuari</Form.Label>
                    <Form.Control type="text" placeholder="Nom d'usuari" />
                </Form.Group>

                <Form.Group controlId="formBasicPassword">
                    <Form.Label>Clau d'accés</Form.Label>
                    <Form.Control type="password" placeholder="Clau" />
                </Form.Group>
                <Form.Group controlId="formConfirmPassword">
                    <Form.Label>Confirmació de clau</Form.Label>
                    <Form.Control type="password" placeholder="Confirmació" />
                </Form.Group>
                <Button variant="outline-dark" type="submit">
                    Enviar
                </Button>
            </Form>
            </div>
        </div>
    )
}
export default Registre