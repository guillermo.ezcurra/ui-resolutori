import * as React from "react"
import './Header.css'
import Button from 'react-bootstrap/Button';
import Navbar from 'react-bootstrap/Navbar';
import FormCheck from 'react-bootstrap/FormCheck';
import Form from 'react-bootstrap/Form';
import Nav  from 'react-bootstrap/Nav';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {useState, useEffect} from "react";
import Modal from 'react-bootstrap/Modal';
import {useHistory} from 'react-router-dom';


function mapStateToProps(state) {
  return {
    nomFitxer: state.fitxers.nom,
    textFitxer: state.fitxers.text,
    nomBD: state.basededades.nomBD,
    urlBD: state.basededades.urlBD,
    usrBD: state.basededades.usrBD,
    clauBD: state.basededades.clauBD,
    literals: state.literals.literals,
    regles: state.regles.regles
  };
}


const Header = (props) => {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  useEffect(() => { 
    props.dispatch({type: "AFEGIR",  nomfitxer: props.nomFitxer, text: props.textFitxer}); 
  });
  //const [seleccionat, setSeleccio] = useState("");
  const handleEsborrament = (event) => {
    console.log(event.target.rgds);
  }
  return (
    <>
    <Navbar bg="dark" variant="dark">
      <Link to='/'>
        <Navbar.Brand >
            <img
            alt=""
            src="./Logo_UPC.svg"
            width="35"
            height="35"
            className="d-inline-block align-top"
          />{' '}
          Resolutori</Navbar.Brand>
      </Link>
      <Nav className="mr-auto">
      </Nav>
      { props.regles.length > 0 ? <Button variant="outline-success mr-1" onClick={handleShow}>Regles</Button> : null }
      <div className="btn-group">
        {/*
        <Link to='/registre'>
          <Button variant="btn btn-link">Registre</Button>
        </Link>
        <Link to='/inicisessio'>
          <Button variant="outline-light">Inici de sessió</Button>
        </Link>
        */}
        
        <Link to={{pathname:'/connexiobd', state: { prevPath: window.location.pathname, nom: '', text: ''}}}>
          <Button variant="outline-light">Connexió BD</Button>
        </Link>
      </div>
    </Navbar>
      <Modal size="lg" show={show} onHide={handleClose}>
          <Modal.Header closeButton>
              <Modal.Title>Regles Definides</Modal.Title>
          </Modal.Header>
          <Form onSubmit={handleEsborrament}>
          <Modal.Body>
          Regles del sistema:
            <Form.Group controlId="rgds">
              {props.regles.map((r) => <FormCheck><FormCheck.Input type="checkbox"></FormCheck.Input><FormCheck.Label>{r}</FormCheck.Label></FormCheck>)}
              </Form.Group>
            </Modal.Body>
            <Modal.Footer>
            
              <Button variant="outline-dark" type="submit">
                  Esborrar
              </Button>
            </Modal.Footer>
          </Form>
      </Modal>
    </>
  )
}
export default connect(mapStateToProps)(Header)