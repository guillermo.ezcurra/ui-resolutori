import React from "react";
import Container from 'react-bootstrap/Container';
import {Editor, EditorState, ContentState } from 'draft-js';
import 'draft-js/dist/Draft.css';
import './PaginaEdicio.css';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import BarraTasques from './BarraTasques/BarraTasques.js';
import { connect } from 'react-redux';
import ListGroup from 'react-bootstrap/ListGroup';
import ArbreFitxers from './Fitxers/ArbreFitxers.js';

const styles = {
    grid: {
        paddingLeft: 0,
        paddingRight: 0
    },
    row: {
        marginLeft: 0,
        marginRight: 0
    },
    col: {
        paddingLeft: 0,
        paddingRight: 0
    },
    colcustom: {
        paddingLeft: 0,
        paddingRight: 0,
        overflow: "auto"
    },
    editor: {
        fontFamily: 'Monospace',
        fontSize: 14
    },
    lit: {
        maxWidth: "462px",
        flexDirection: 'column'
    }
};

function mapStateToProps(state) {
    return {
      nomFitxer: state.fitxers.nom,
      textFitxer: state.fitxers.text,
      nomBD: state.basededades.nomBD,
      urlBD: state.basededades.urlBD,
      usrBD: state.basededades.usrBD,
      clauBD: state.basededades.clauBD,
      literals: state.literals.literals,
      regles: state.regles.regles
    };
  }
  
class PaginaEdicio extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            editorState: EditorState.createWithContent(ContentState.createFromText(this.props.textFitxer)),
            //text: this.props.location.state.fitxerContingut,
            text: this.props.textFitxer,
            nom: this.props.nomFitxer,
            informacio: false,
            seleccio: '',
            literals: [],
            reglesNoves: [],
            literalsNous: [],
            reparacions: null,
            llistesReparacions: []
        }
    }
    onChange = (editorState) => {
        this.setState({editorState});
        
    }
    componentDidMount() {
        this.props.dispatch({type: "AFEGIR",  nomfitxer: this.props.nomFitxer, text:this.props.textFitxer});
    }

    componentWillUnmount() {
        this.props.dispatch({type: "AFEGIR",  nomfitxer: this.props.nomFitxer, text:this.state.editorState.getCurrentContent().getPlainText()});
    }

    render() {
        return(
            <div className="eines">
                <BarraTasques dadesBD={this}/>
                <Row style={styles.row}>
                    <Col style={styles.col}>
                    <Container  className="mt-2">
                        
                        <ArbreFitxers dadesFitxer={this.props.nomFitxer}/>
                    </Container>
                    
                    </Col>
                    <Col xs={6} style={styles.col}>

                        <Container className="editor mt-2">
                            <div style={styles.editor}>
                                <Editor 
                                    editorState={this.state.editorState}
                                    onChange= {this.onChange}
                                    /> 
                            </div>
                        </Container>
                        
                    </Col>
                    <Col style={styles.colcustom}>
                        <Container className="llistaDades mt-2">
                            Dades esquema lògic
                            <div>
                            {/*  */}
                            <ListGroup>
                            {this.props.literals.map((lit,j) => <div class="list-group-item flex-column text-truncate" style={{display: "flex"}} key={j}>{lit}</div>)}
                            {/* {this.props.literals.map((lit,j) => <ListGroup.Item key={j}>{lit.atom.predicateName}({lit.atom.terms.map((t,i) =>(i === (lit.atom.terms.length - 1)) ? <div class="terme">{t.name}</div> : <div class="terme">{t.name},</div>)})</ListGroup.Item>)} */}
                            </ListGroup>
                            </div>  
                        </Container>
                    </Col>
                </Row>
                
            </div>
        )
    }
}

export default connect(mapStateToProps)(PaginaEdicio);