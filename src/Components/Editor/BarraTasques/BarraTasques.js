import * as React from "react";
//import * as d3 from 'd3-hierarchy';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import 'boxicons';
//import Container from 'react-bootstrap/Container';
//import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Modal from 'react-bootstrap/Modal';
import {useState} from "react";
import ListGroup from 'react-bootstrap/ListGroup';
import './BarraTasques.css';
import Tree from 'react-tree-graph';
import 'react-tree-graph/dist/style.css'
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
const styles = {
    navegacio: {
        paddingTop: 5,
        paddingBottom: 0
    },
}



function handleDownload(props){
    const axios = require('axios');
    if(props.dadesBD.props.urlBD === '') alert("Cal que et connectis primer a una base de dades.");
    else {
        var base64 = require('base-64');
        var clauCodificada = base64.encode(props.dadesBD.props.clauBD);
        axios.get(`http://localhost:8080/api/esquema`, {
            params: {
                nomBD: props.dadesBD.props.nomBD,
                URL: props.dadesBD.props.urlBD,
                username: props.dadesBD.props.usrBD,
                clau: clauCodificada,
            } 
        }).then(function (response) {
            console.log(response);
            props.dadesBD.props.dispatch({type: "AFEGIRLITERALS",  literals: response.data.second});
        });
    }
    
}

function handleUpload(props){
    //const axios = require('axios');
    if(props.dadesBD.props.urlBD === '') alert("Cal que et connectis primer a una base de dades.");
    else {
        //axios.get(`http://localhost:8080/api/esquema`, {
        //    params: {
        //        nomBD: props.dadesBD.props.nomBD,
        //        URL: props.dadesBD.props.urlBD,
        //        username: props.dadesBD.props.usrBD,
        //        clau: props.dadesBD.props.clauBD,
        //    } 
        //}).then(function (response) {
        //    console.log(response);
        //    props.dadesBD.props.dispatch({type: "AFEGIRLITERALS",  literals: response.data});
        //});
    }
    
}

async function handleExecucio(props) {
    const axios = require('axios');
    var selectionState = props.dadesBD.state.editorState.getSelection();
    var startKey = selectionState.getStartKey();
    var endKey = selectionState.getEndKey();
    var currentContent = props.dadesBD.state.editorState.getCurrentContent();
    var currentContentBlock = currentContent.getBlockForKey(startKey);
    var selectedText = [];
    var key = startKey;
    var trobat = false;
    while (!(trobat)) {
        currentContentBlock = currentContent.getBlockForKey(key);
        selectedText.push(currentContentBlock.getText());
        if (key === endKey) trobat = true;
        key = currentContent.getKeyAfter(key);
    }

    //Codifiquem la clau de l'usuari de la BD.
    var base64 = require('base-64');
    var clauCodificada = base64.encode(props.dadesBD.props.clauBD);
    if(props.dadesBD.props.urlBD === '') {
        alert("Cal que et connectis primer a una base de dades.");
        return false;
    }
    else {
        await axios({
            method: 'post',     //put
            url: `http://localhost:8080/api/esquema/regles`,
            params: {
                nomBD: props.dadesBD.props.nomBD,
                URL: props.dadesBD.props.urlBD,
                username: props.dadesBD.props.usrBD,
                clau: clauCodificada,
            },
            data: {
               reglesNoves: selectedText
            }
          }).then(function(response) {
            response.data.map((d) => props.dadesBD.state.reglesNoves.push(d));
        });
        return true;
    }
}
async function handleEsdeveniments(props) {
    const axios = require('axios');
    var selectionState = props.dadesBD.state.editorState.getSelection();
    var startKey = selectionState.getStartKey();
    var endKey = selectionState.getEndKey();
    var currentContent = props.dadesBD.state.editorState.getCurrentContent();
    var currentContentBlock = currentContent.getBlockForKey(startKey);
    var selectedText = [];
    var key = startKey;
    var trobat = false;
    while (!(trobat)) {
        currentContentBlock = currentContent.getBlockForKey(key);
        selectedText.push(currentContentBlock.getText());
        if (key === endKey) trobat = true;
        key = currentContent.getKeyAfter(key);
    }

    //Codifiquem la clau de l'usuari de la BD
    if(props.dadesBD.props.urlBD === '') {
        alert("Cal que et connectis primer a una base de dades.");
        return false;
    }
    else {
        await axios({
            method: 'post',     //put
            url: `http://localhost:8080/api/esquema/chase`,
            data: {
                literals: props.dadesBD.props.literals,
                nousLiterals: selectedText,
                regles : props.dadesBD.props.regles
            }
          }).then(function(response) {
            //console.log(response.data);
            
            props.dadesBD.state.reparacions = response.data.first;
            props.dadesBD.state.llistesReparacions = response.data.second;
            //let reparacions = d3.hierarchy(response.data[, fills]);
            //response.data.map((d) => props.dadesBD.state.reglesNoves.push(d));
        });
        return true;
    }
}

const BarraTasques = (props) => {
    const [carregaDades, setVeureDades] = useState(false);
    const handleTancarDades = () => {
        setVeureDades(false);
        props.dadesBD.state.reglesNoves = [];
    }
    const handleVeureDades = () => setVeureDades(true);
    const [show, setShow] = useState(false);
    const [numReparacio, setNum] = useState(0);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const [carregaReparacions, setVeureReparacions] = useState(false);
    const handleVeureReparacions = () => setVeureReparacions(true);
    const handleTancarReparacions = () => setVeureReparacions(false);
    const marges = { bottom : 10, left : 20, right : 250, top : 10};
    const clicExecucio = async (props) => {
        var resposta = false;
        resposta = await handleExecucio(props);
        if(resposta) {
                props.dadesBD.props.dispatch({type: "AFEGIRREGLES",  regles: props.dadesBD.state.reglesNoves});
                handleVeureDades();
        }
    };
    const clicEsdeveniments = async (props) => {
        var resposta = false;
        resposta = await handleEsdeveniments(props);
        if(resposta) {
                //props.dadesBD.props.dispatch({type: "AFEGIRLITERALS",  literals: props.dadesBD.state.literalsNous});
                handleVeureReparacions();
        }
    };
    const onSubmit = () => {
        const rep = props.dadesBD.state.llistesReparacions[numReparacio-1]
        props.dadesBD.props.dispatch({type: "AFEGIRLITERALS",  literals: rep});
        props.dadesBD.state.llistesReparacions = [];
    };

    return (
        <div>
        <Navbar bg="light" variant="light" style={styles.navegacio} className="border-bottom border-dark">
            
            
            <Col fluid >
                <Nav >
                    <Nav.Item >
                        {/* <box-icon class="icon" name='file-blank' title="Crear fitxer en blanc"></box-icon>
                        <box-icon class="icon" name='file-find'title="Obrir fitxer"></box-icon> */}
                    </Nav.Item>
                </Nav>
            </Col>
            <Col xs={3} fluid>
                <Nav>
                    <Nav.Item >
                        <box-icon class="icon" name="play" title="Afegeix les regles" onClick={() =>clicExecucio(props)}></box-icon>
                        {/*<box-icon class="icon" name="save" title="Desa el fitxer de treball"></box-icon>*/}
                        <box-icon class="icon" name='plus' title="Afegeix els esdeveniments" onClick={() =>clicEsdeveniments(props)}></box-icon>
                    </Nav.Item>
                </Nav>
            </Col>
            <Col xs={3} fluid>
                <Nav className='justify-content-end'>
                    <Nav.Item >
                        {/* Aquí hi posariem els botons que calguessin*/}
                    </Nav.Item>
                </Nav>
            </Col>
            <Col fluid>
            <Nav>
                    <Nav.Item >
                        <box-icon class="icon" name="data" title="Dades de la connexió" onClick={handleShow}></box-icon>
                        <box-icon class='icon' name='download' title="Descarrega les dades de la base de dades" onClick={() =>handleDownload(props)}></box-icon>
                        <box-icon class='icon' name='upload' title="Carrega les noves dades a la base de dades" onClick={() =>handleUpload(props)}></box-icon>  
                    </Nav.Item>
                </Nav>
            </Col>
            
        
            
                
        </Navbar>
        <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>Dades de la BD</Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <ListGroup variant="flush">
                <ListGroup.Item><b>Nom:</b> {props.dadesBD.props.nomBD}</ListGroup.Item>
                <ListGroup.Item><b>URL:</b> {props.dadesBD.props.urlBD}</ListGroup.Item>
                <ListGroup.Item><b>Nom d'usuari:</b>  {props.dadesBD.props.usrBD}</ListGroup.Item>
            </ListGroup>
            </Modal.Body>
        </Modal>
        <Modal size="lg" show={carregaDades} onHide={handleTancarDades}>
            <Modal.Header closeButton>
                <Modal.Title>Noves regles</Modal.Title>
            </Modal.Header>
            <Modal.Body>
            Hem afegit aquestes regles al sistema:
            <ul>
             {props.dadesBD.state.reglesNoves.map((r) => <li>{r}</li>)}
            </ul>  
            </Modal.Body>
        </Modal>
        <Modal size="xl" show={carregaReparacions} onHide={handleTancarReparacions}>
            <Modal.Header closeButton>
                <Modal.Title>Reparacions</Modal.Title>
            </Modal.Header>
            <Modal.Body>
            {/* <Tree
            data={props.dadesBD.state.reparacions}
            height={200}
            width={1050}

            margins={marges}/> */}
            <ol>
             {props.dadesBD.state.llistesReparacions.map((r) => <li>{r}</li>)}
            </ol> 
            </Modal.Body>
            <Modal.Footer>
            <Form>
            <Form.Row>
                <Form.Control placeholder="Tria la reparació" onChange={e => setNum(e.target.value)} />
              <Button variant="outline-dark" onClick={onSubmit}>
                  Aplicar
              </Button>
              
              </Form.Row>
              </Form>
            </Modal.Footer>
        </Modal>

        {/* <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>Dades de la BD</Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <ListGroup variant="flush">
                <ListGroup.Item><b>Nom:</b> {props.dadesBD.props.nomBD}</ListGroup.Item>
                <ListGroup.Item><b>URL:</b> {props.dadesBD.props.urlBD}</ListGroup.Item>
                <ListGroup.Item><b>Nom d'usuari:</b>  {props.dadesBD.props.usrBD}</ListGroup.Item>
            </ListGroup>
            </Modal.Body>
        </Modal> */}

        </div>
    )
}
export default BarraTasques