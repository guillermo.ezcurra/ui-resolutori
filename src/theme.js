import { red } from '@material-ui/core/colors';
import { createMuiTheme } from '@material-ui/core/styles';
// A custom theme for this app
const theme = createMuiTheme({
  palette: {
    type: 'light',
    // color verd 
    primary: {
      main: '#4CAF50',
      light: '#81C784',
      dark: '#1B5E20',
    },
    secondary: {
      main: '#9E9E9E',
      light: '#BDBDBD',
      dark: '#212121',
    },
    error: {
      main: red.A400,
    },
    background: {
      default: '#0E1B00',
    },
  },
  overrides: {
    MuiPaper: {
      root: {
        padding: '20px 10px',
        margin: '10px',
        backgroundColor: '#fff', // 5d737e
      },
    },
    MuiButton: {
      root: {
        margin: '5px',
      },
    },
    MuiToolbar: {
      root: {
        maxheight: 48, // Instead of 64
      },
    },
  },
});
export default theme;