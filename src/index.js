import React from 'react';
import ReactDOM from 'react-dom';
import App from './Components/App/App';
import Header from "./Components/Header/Header";
import { BrowserRouter as Router } from 'react-router-dom';
// És el fitxer personalitzat amb el bootsrap
import './index.scss';
import { combineReducers, createStore } from 'redux';
import { Provider } from 'react-redux';

const initialState = {
  nom: '',
  text: '',
  nomBD: '',
  urlBD: '',
  usrBD: '',
  clauBD: '',
  literals: [],
  regles: []
};

function fitxers(state = initialState, action) {
  switch(action.type) {
    case 'MODIFICAR' :
      return {
        text: action.text
      };
    case 'AFEGIR':
      return {
        nom: action.nomfitxer,
        text: action.text
      };
    case 'RESET':
      return {
        nom: '',
        text: ''
      };
    default:
      return state;
  }
};

function basededades(state = initialState, action) {
  switch(action.type) {
    case 'AFEGIRBD':
      return {
        nomBD: action.nomBD,
        urlBD: action.urlBD,
        usrBD: action.usrBD,
        clauBD: action.clauBD
      };
    default:
      return state;
  }
}

function literals(state = initialState, action) {
  switch(action.type) {
    case 'AFEGIRLITERALS':
      return {
        ...state,
        literals: state.literals.concat(action.literals),
      };
    default:
      return state;
  }
}

function regles(state  = initialState, action) {
  switch(action.type) {
    case 'AFEGIRREGLES':
      return {
        ...state,
        regles: state.regles.concat(action.regles),
      };
    default:
      return state;
  }
}

const reducers = combineReducers({
  fitxers,
  basededades,
  literals,
  regles
})
const store = createStore(reducers);
//store.dispatch({type: "AFEGIR",  nomfitxer: "dispatch", text:"prova"});
//store.dispatch({type: "RESET"});

ReactDOM.render(
  <Router>
    <Provider store={store}>
      <Header />
      <App />  
    </Provider>  
  </Router>,
  document.getElementById('root'),
);




// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
//reportWebVitals();
